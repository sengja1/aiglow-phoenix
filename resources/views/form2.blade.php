@extends('layouts.layoutform')
@section('content')
<img src="/img/imgform1.png" alt="form logo picture" class="responsive" >


<div class="container">
<!-- Success message -->
@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif
<h1 style="text-align:center">ヒアリング担当者</h1>

<form action="/form2" method="POST" >
    @csrf

 <div class="form-group">
    <label>担当 AT</label>
    <select name="AT" id="AT">
    　　<option value="担当 AT1">担当 AT1</option>
        <option value="担当 AT2">担当 AT2</option>
        <option value="担当 AT3">担当 AT3</option>
        <option value="担当 AT4">担当 AT4</option>
        <option value="担当 AT5">担当 AT5</option>
        <option value="担当 AT6">担当 AT6</option>
        <option value="担当 AT7">担当 AT7</option>
    </select>
 </div>

 <div class="form-group">
    <label>担当 AR</label>
    <select name="AR" id="AR">
    　　<option value="担当 AR1">担当 AR1</option>
        <option value="担当 AR2">担当 AR2</option>
        <option value="担当 AR3">担当 AR3</option>
        <option value="担当 AR4">担当 AR4</option>
        <option value="担当 AR5">担当 AR5</option>
        <option value="担当 AR6">担当 AR6</option>
        <option value="担当 AR7">担当 AR7</option>
    </select>
 </div>

<div class="form-group">
    <label>顧客担当者名</label>
    <select name="Customer" id="Customer">
    　　
        <option value="顧客担当者名1">顧客担当者名1</option>
        <option value="顧客担当者名2">顧客担当者名2</option>
        <option value="顧客担当者名3">顧客担当者名3</option>
        <option value="顧客担当者名4">顧客担当者名4</option>
        <option value="顧客担当者名5">顧客担当者名5</option>
        <option value="顧客担当者名6">顧客担当者名6</option>
        <option value="顧客担当者名7">顧客担当者名7</option>
    </select>
 </div>
 

<input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">

</form>
</div>
@endsection

