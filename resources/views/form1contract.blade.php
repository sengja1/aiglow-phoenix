@extends('layouts.layoutform')
@section('content')
<img src="/img/imgform1.png" alt="form logo picture" class="responsive" >


<div class="container">
<!-- Success message -->
@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

<h1 style="text-align:center" >契約概要</h1>

<form action="/form1contract" method="POST" >
    @csrf


  <div class="form-group">
        <label>name</label>
        <input type="text"  name="name" id="name" >
 </div>


 <div class="form-group">
    <label>契約ステータス</label>
    <select name="status" id="status">
    　　<option value="TEL">TEL</option>
        <option value="名刺交換済み">名刺交換済み</option>
        <option value="関係構築中">関係構築中</option>
        <option value="商談化">商談化</option>
        <option value="商談">商談</option>
        <option value="契約締結中">契約締結中</option>
        <option value="商談">Prj.開始待ち</option>
        <option value="Close">Close</option>
    </select>
 </div>

 <div class="form-group">
        <label>案件名</label>
        <input type="text"  name="prj-title" id="prj-title" >
 </div>

  <div class="form-group">
        <label>業務要件種別</label>
        <select name="prj-type" id="prj-type"> 
          <option value="bsn1">bsn1</option>
          <option value="bsn2">bsn2</option>
          <option value="bsn3">bsn3</option>
          <option value="bsn4">bsn4</option>
        </select>
  </div>

  <div class="form-group" >
          <label>契約期間</label>
          <label for="start">start:</label>
            <input type="date" id="startdate" name="startdate">
          <label for="end">end:</label>
            <input type="date" id="enddate" name="enddate">
  </div>

  <div class="form-group">
        <label>顧客名</label>
        <input type="text"  name="customer" id="customer" >
  </div>

  <div class="form-group">
        <!-- <label>予算</label> -->
           <!-- <input type="text" name="budjet" id="budjet" > -->
         
  <!-- <input type="number" name="budjet" id="budjet" min="10" max="1000000000" > -->
  <label for="txtNum">予算 </label>
  <input type="text" name="budjet" id="budjet" onkeyup="format(this)" />
  </div>
<script>
function format(input) {
  var nStr = input.value + '';  
  nStr = nStr.replace(/\,/g, "");
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';  
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  input.value = x1 + x2;
}
</script>


<!-- <script type="text/javascript" src="jquery.js"></script>
        <script type="text/javascript" src="jquery.numberformatter.js"></script>
        <script>
        $(document).ready(function() {
            $(".numbers").each(function() {
                $(this).format({format:"#,###", locale:"us"});
            });
        });
        </script>
         -->


  <div class="form-group">  
            <label>通貨単位</label>
           <select name="currency" id="currency"> 
              <option value="日本円">日本円</option>
              <option value="米ドル">米ドル</option>
              <option value="人民元">人民元</option>
           </select>
 </div>


  <div class="form-group">
        <label>契約形態種類</label>
        <select name="contract-type" id="contract-type"> 
          <option value="派遣契約">派遣契約</option>
          <option value="委託契約">委託契約</option>
          <option value="準委任契約">準委任契約</option>
          <option value="請負契約">請負契約</option>
        </select>
  </div>

  <div class="form-group">
        <label>エンド企業名</label>
        <input type="text" name="b-name" id="b-name" >
        
  </div>



  <div class="form-group">
        <label>エンド業種</label>
      <select name="b-type" id="b-type"> 
          <option value="営業">営業</option>
          <option value="販売">販売</option>
          <option value="不動産業">不動産業</option>
          <option value="医療">医療</option>
      </select>
  </div>

  <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
</form>

</div>
@endsection

