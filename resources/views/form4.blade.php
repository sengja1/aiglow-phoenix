@extends('layouts.layoutform')
@section('content')
<img src="/img/imgform1.png" alt="form logo picture" class="responsive" >


<div class="container">
<!-- Success message -->
@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif
<h1 style="text-align:center">ATヒアリング</h1>

<form action="/form4" method="POST" >
    @csrf

  <div class="form-group">
        <label>ATヒアリング日付</label>
        <input type="date"  name="date" id="date" >
 </div>

 <div class="form-group">
        <label>所属会社</label>
      <select name="com" id="com">
    　　<option value="所属会社1">所属会社1</option>
        <option value="所属会社2">所属会社2</option>
        <option value="所属会社3">所属会社3</option>
        <option value="所属会社4">所属会社4</option>
       </select>
 </div>
 

 <div class="form-group">
        <label>社員番号</label>
        <input type="text"  name="e-num" id="e-num" >
 </div>

 <div class="form-group">
        <label>名前</label>
        <input type="text"  name="name" id="name" >
 </div>

 <div class="form-group">
        <label>フリガナ</label>
        <input type="text"  name="kana" id="kana" >
 </div>

 <div class="form-group">
        <label>プロジェクト背景</label>
        <input type="text"  name="pjbackground" id="pjbackground" >
 </div>

 <!-- ATが1名→PJ種別→自分の業務　として Dev. を選ぶ、が出来る。
ATが2名以上を想定して、大項目・中項目の書き方から返る必要あり。
左記の 縦に並ぶ列は、それぞれ小分類で横に横に並べるのがベターか。 -->

 <!-- 求められている役割/PJ種別 -->
 <div class="form-group">
        <label>役割又PJ種別</label>
        <select name="projecttype" id="projecttype"> 
              <option value="Dev">Dev</option>
              <option value="Infra">Infra</option>
              <option value="Spt">Spt</option>
              <option value="Hlpd">Hlpd</option>
              <option value="More">More</option>
       </select>
 </div>

  <!-- put version or not -->
 <div class="form-group">
        <label>使用OS</label>
        <select name="os" id="os"> 
              <option value="Windows Server">Windows Server</option>
              <option value="Mac">Mac</option>
              <option value="Linux Distribution ">Linux Distribution </option>
              <option value="More">More</option>
      </select>
 </div>


 <div class="form-group">
        <label>DB</label>
        <select name="db" id="db"> 
              <option value="MYSQL">MYSQL</option>
              <option value="RDMS">RDMS</option>
              <option value="Desktop ">Desktop </option>
              <option value="More">More</option>
      </select>
 </div>
 
<!-- tag を付ける考え方がよい？																		
使用経験があるOSが1つなら、Input も Serch も Filter もカンタン。																		
使用経験があるOSが2つ、3つ、4つあれば、DBにどうデータを持たせればデータポータルで検索しやすいか？																		
Google データポータルの仕組みから、適切なData Input 方法をする必要がある。							 -->

 <div class="form-group">
        <label>ツール</label>
        <select name="tool" id="tool"> 
              <option value="tool1">tool1</option>
              <option value="tool2">tool2</option>
              <option value="tool3">tool3 </option>
              <option value="tool4">tool4</option>
      </select>
 </div>
 

<input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
</form>
</div>
@endsection

