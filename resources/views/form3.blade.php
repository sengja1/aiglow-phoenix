@extends('layouts.layoutform')
@section('content')
<img src="/img/imgform1.png" alt="form logo picture" class="responsive" >


<div class="container">
<!-- Success message -->
@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif
<h1 style="text-align:center">商流</h1>

<form action="/form3" method="POST" >
@csrf
 
<div class="form-group">
    <label>自社</label>
    <select name="company" id="company">
    　　<option value="元請">元請</option>
        <option value="1次">1次</option>
        <option value="2次">2次</option>
        <option value="3次">3次</option>
        <option value="4次">4次</option>
     </select>
</div>

 <div class="form-group">
        <label>1次受社名</label>
        <input type="text" class="form-control " name="sub-1" id="sub-1" >
 </div>

 <div class="form-group">
        <label>2次請社名</label>
        <input type="text" class="form-control " name="sub-2" id="sub-2" >
 </div>

 <div class="form-group">
        <label>3次請社名</label>
        <input type="text" class="form-control " name="sub-3" id="sub-3" >
 </div>


<input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
</form>
</div>
@endsection

