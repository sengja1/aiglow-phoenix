<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForm1contractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form1contracts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('contract_status');
           $table->string('case');
           $table->string('bus_req_type');
           $table->string('con_start');
           $table->string('con_end');
           
            $table->string('cus_name');
            $table->string('budjet');
            $table->string('currency_unit');
            $table->string('type_of_contract');
            $table->string('end_bus_name');
            $table->string('end_industry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form1contracts');
    }
}
