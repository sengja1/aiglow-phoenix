<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForm4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form4s', function (Blueprint $table) {
            $table->id();
            $table->string('ATヒアリング日付');
            $table->string('所属会社');
            $table->string('社員番号');
            $table->string('名前');
            $table->string('フリガナ');
            $table->string('プロジェクト背景');
            $table->string('役割又PJ種別');
           
            $table->string('使用OS');
            $table->string('DB');
            $table->string('ツール');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form4s');
    }
}
