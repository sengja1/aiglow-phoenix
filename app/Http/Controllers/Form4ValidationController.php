<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form4;

class Form4ValidationController extends Controller
{
// Create Form4
public function createUserForm4(Request $request) {
    return view('form4');
}
// Store Form4 data in database
public function store() {
    $form4 = new form4();
    
    $form4->ATヒアリング日付=request('date');
    $form4->所属会社 =request ('com');
    $form4->社員番号=request ('e-num');
    $form4->名前 =request('name');
    $form4->フリガナ=request('kana');
    $form4->プロジェクト背景 =request ('pjbackground');
    $form4->役割又PJ種別=request ('projecttype');
    $form4->使用OS =request('os');
    $form4->DB =request ('db');
    $form4->ツール=request ('tool');
   
    
   $form4->save();
    error_log($form4);
   // return redirect('/');
   return back()->with('success', 'Your form has been submitted.');
    
   }
   
   }

