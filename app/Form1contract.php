<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form1contract extends Model
{
    public $fillable = ['name', 'contract_status', 'case','bus_req_type','con_start',
    'con_end','cus_name','budjet','currency_unit','type_of_contract','end_bus_name','end_industry'];

}
