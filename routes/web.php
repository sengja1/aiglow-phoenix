<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');
});


Route::get('/form1contract', 'Form1contractValidationController@createUserForm1contract');
Route::post('/form1contract', 'Form1contractValidationController@store');

Route::get('/form2', 'Form2ValidationController@createUserForm2');
Route::post('/form2', 'Form2ValidationController@store');

Route::get('/form3', 'Form3ValidationController@createUserForm3');
Route::post('/form3', 'Form3ValidationController@store');

Route::get('/form4', 'Form4ValidationController@createUserForm4');
Route::post('/form4', 'Form4ValidationController@store');


